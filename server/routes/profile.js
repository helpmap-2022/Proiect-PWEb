const express = require('express')

const router = express.Router()
const {
  createProfile,
  getAllProfiles,
  getProfile,
  updateProfile,
    loginProfile,
} = require('../controllers/profile')

router.route('/').post(createProfile).get(getAllProfiles)

router.route('/:id').get(getProfile).patch(updateProfile)

router.route("/:email").get(loginProfile);

module.exports = router