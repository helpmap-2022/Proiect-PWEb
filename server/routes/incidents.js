const express = require('express')

const router = express.Router()
const {
  createIncident,
  getAllIncidents,
  updateIncident,
  getIncident,
} = require('../controllers/incidents')

router.route('/').post(createIncident).get(getAllIncidents)

router.route('/:id').get(getIncident).patch(updateIncident)

module.exports = router