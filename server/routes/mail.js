const express = require('express')

const router = express.Router()
const {
  publish
} = require('../controllers/message')

router.route('/').post(publish)

module.exports = router