const mongoose = require('mongoose')

const IncidentSchema = new mongoose.Schema({
    reporter: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: [true, 'Please provide user'],
    },
    message: {
        type: String,
        maxLength: 100,
        required: true,
    },
    incType: {
        type: String,
        maxLength: 50,
        require: true,
    },
    position: {
        lat: { 
            type: Number,
            require: true,
        },
        long: {
            type: Number,
            require: true,
        }
    },
    nLikes: {
        type: Number,
    },
    nDislikes: {
        type: Number,
    }
})

module.exports = mongoose.model('Incident', IncidentSchema)
