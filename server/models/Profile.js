const mongoose = require('mongoose')

const ProfileSchema = new mongoose.Schema({
    name: {
        type: String,
        maxlength: 50,
        minlength: 3,
    },
    email: {
        type: String,
        required: [true, 'Please provide email'],
        match: [
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        'Please provide a valid email',
        ],
        unique: true,
    },
    password: {
        type: String,
        required: [true, 'Please provide password']
    },
    phoneNumber: {
        type: String,
        uniqe: true,
    },
    rights: {
        type: Number,
        require: true
    },
    address: {
        lat: { 
            type: Number
        },
        long: {
            type: Number
        }
    },
    ID: {
      type: Buffer
    }
  })

  module.exports = mongoose.model('Profile', ProfileSchema)