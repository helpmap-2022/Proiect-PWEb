const dotenv = require('dotenv');
const EmailService = require('../controllers/mail.service');
const { StatusCodes } = require('http-status-codes')
const Profile = require('../models/Profile')
const getDistance = require('../middleware/distance')

const queue = 'email-task';

const open = require('amqplib').connect(process.env.AMQP_SERVER)

// Publisher
const publishMessage = payload => open.then(connection => connection.createChannel())
  .then(channel => channel.assertQueue(queue)
    .then(() => channel.sendToQueue(queue, Buffer.from(JSON.stringify(payload)))))
  .catch(error => console.warn(error));

// Consumer
const consumeMessage = () => {
    open.then(connection => connection.createChannel()).then(channel => channel.assertQueue(queue).then(() => {
      console.log(' [*] Waiting for messages in %s. To exit press CTRL+C', queue);
      return channel.consume(queue, async (msg) => {
        if (msg !== null) {
          const { sendTo, message } = JSON.parse(msg.content.toString());  
          console.log(' [x] Received %s', sendTo, message);
          EmailService.sendMail(sendTo, message)
        } 
      });
    })).catch(error => console.warn(error));
};

module.exports = {
    publishMessage,
    consumeMessage 
  }

  require('make-runnable');

