const User = require('../models/User')
const jwt = require('jsonwebtoken')

const auth = async (req, res, next) => {
    const authHead = req.headers.authorization
    if (!authHead || !authHead.startsWith('Bearer')) {
        throw new Error('Authentication failed')
    }
    const token = authHead.split(' ')[1]

    try {
        const payload = jwt.verify(token, process.env.JWT_SECRET)
        req.user = { userId: payload.userId, name: payload.name }
        next()
    } catch (error) {
        throw new Error('Authentication failed')
    }
}

module.exports = auth
