const Profile = require('../models/Profile')
const { StatusCodes } = require('http-status-codes')

const getAllProfiles = async (req, res) => {
    const profiles = await Profile.find({})
    res.status(StatusCodes.OK).json({ profiles, count: profiles.length })
}

const getProfile = async (req, res) => {

    const profile = await Profile.find({ 
        email: req.body.email,
    })

    if (!profile) throw new Error(`Profile with $(req.params.id) not fund`)

    res.status(StatusCodes.OK).json({ profile })
}

const createProfile = async (req, res) => {
    const profile = await Profile.create(req.body)
    res.status(StatusCodes.CREATED).json({ profile })
}

const updateProfile = async (req, res) => {
    const profile = await Profile.findByIdAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true, runValidators: true}
    )

    if (!profile) throw new Error(`No profile with id ${req.body.id}`)

    res.status(StatusCodes.OK).json({ profile })
}

const loginProfile = async (req, res) => {
    const profile = await Profile.find({
        email: req.body.email,
    })

    if (!profile) throw new Error(`Profile with $(req.params.email) not fund`)

    console.log(profile);
    res.status(StatusCodes.OK).json({ profile })
}

module.exports = {
    getAllProfiles,
    getProfile,
    createProfile,
    updateProfile,
    loginProfile
  }