const { publishMessage } = require('../middleware/mailWorker')
const { StatusCodes } = require('http-status-codes')

const publish = (req, res) => {
    publishMessage(req.body)
    return res.status(StatusCodes.OK).send({ 
        message: 'email sent succesfully'
    })
}

module.exports = {
    publish
}
