const Incident = require('../models/Incidents')
const Profile = require('../models/Profile')
const { publishMessage } = require('../middleware/mailWorker')
const { StatusCodes } = require('http-status-codes')
const getDistance = require('../middleware/distance')

const getAllIncidents = async (req, res) => {
    const incidents = await Incident.find({})
    res.status(StatusCodes.OK).json({ incidents, count: incidents.length })
}

const getIncident = async (req, res) => {

    const incident = await Incident.find({ 
        _id: req.params.id,
    })

    if (!incident) throw new Error(`Incident with $(req.params.id) not fund`)

    res.status(StatusCodes.OK).json({ incident })
}

const createIncident = async (req, res) => {
    const reporterProfile = await Profile.find({
        email: req.body.reporter
    })
    req.body.reporter = reporterProfile[0]._id
    const incident = await Incident.create(req.body)
    const profiles = await Profile.find({})
    let mails = []
    profiles.forEach(profile => {
        const position = JSON.parse(req.body.position)
        const distance = getDistance(
            profile.address.lat,
            position.lat,
            profile.address.long,
            position.long
        )
        if (Math.abs(distance) < 10000) {
            mails.push(profile.email)
        }
    })

    const sendTo = mails.join(', ')
    console.log(sendTo)
    publishMessage({sendTo: sendTo, message: req.body.message})
    res.status(StatusCodes.CREATED).json({ incident })
}

const updateIncident = async (req, res) => {
    const incident = await Incident.findByIdAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true, runValidators: true}
    )

    if (!incident) throw new Error(`No incident with id ${req.body.id}`)

    res.status(StatusCodes.OK).json({ incident })
}

module.exports = {
    createIncident,
    getAllIncidents,
    updateIncident,
    getIncident,
  }