require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
require('./db/conn')
const app = express()

app.use(cors());

const port = process.env.PORT || 5000

const incidentRouter = require('./routes/incidents')
const profileRouter = require('./routes/profile')
const mailRouter = require('./routes/mail')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send("Hello world")
})

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`)
})

app.use('/api/incidents', incidentRouter)
app.use('/api/profiles', profileRouter)
app.use('/api/mail', mailRouter)
