import React, {Component} from 'react';
import {Map, GoogleApiWrapper, Marker, InfoWindow} from 'google-maps-react-17';
import MenuIncidents from "../components/MenuIncidents";
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import ThumbDownIcon from '@mui/icons-material/ThumbDown';
import {Button} from "@mui/material";
import axios from 'axios';

const containerStyle = {
    position: 'absolute',
    width: '100%',
    height: '93%'
}

var markers = [];

axios.get(process.env.REACT_APP_BACKEND_URL + ":" + process.env.REACT_APP_BACKEND_PORT +"/api/incidents").then(response => {
    markers = response.data.incidents;
});

export class MapContainer extends Component {
    constructor(props) {
        super(props);
        this.state ={
            showingInfoWindow: false,
            activeMarker: {},
            selectedPlace: {},
        }
    }

    onMarkerClick = (props, marker, e) =>
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
        });

    onMapClicked = (props) => {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            })
        }
    };

    handleLike = () => {


        //axios.patch("http://localhost:5000/api/incidents/" + this.state.selectedPlace._id, {"nLikes" : this.state.selectedPlace.likes++}).then(res => console.log(res));
    }

    handleDislike = () => {
        //axios.patch("http://localhost:5000/api/incidents/" + this.state.selectedPlace._id, {"nDislikes" : this.state.selectedPlace.dislikes++}).then();
    }

    renderSwitch = (marker) => {
        switch(marker.incType) {
            case 'hospital':
                return <Marker
                    key={marker._id}
                    onClick={this.onMarkerClick}
                    name={marker.message}
                    likes={marker.nLikes}
                    dislikes={marker.nDislikes}
                    position={new this.props.google.maps.LatLng(marker.position.lat, marker.position.long)}
                    icon={{
                        url: '/red_cross.svg',
                        scaledSize: new window.google.maps.Size(25, 25)
                    }}
                />;
            case 'allies':
                return <Marker
                key={marker._id}
                onClick={this.onMarkerClick}
                name={marker.message}
                likes={marker.nLikes}
                dislikes={marker.nDislikes}
                position={new this.props.google.maps.LatLng(marker.position.lat, marker.position.long)}
                icon={{
                    url: '/allies-troops.svg',
                    scaledSize: new window.google.maps.Size(50, 50)
                }}
            />;
            case 'enemies':
                return <Marker
                    key={marker._id}
                    onClick={this.onMarkerClick}
                    name={marker.message}
                    likes={marker.nLikes}
                    dislikes={marker.nDislikes}
                    position={new this.props.google.maps.LatLng(marker.position.lat, marker.position.long)}
                    icon={{
                        url: '/enemies-troops.svg',
                        scaledSize: new window.google.maps.Size(50, 50)
                    }}
                />;
            default:
                return <Marker
                    key={marker._id}
                    onClick={this.onMarkerClick}
                    name={marker.message}
                    likes={marker.nLikes}
                    dislikes={marker.nDislikes}
                    position={new this.props.google.maps.LatLng(marker.position.lat, marker.position.long)}
                    icon={{
                        url: '/safehouse.svg',
                        scaledSize: new window.google.maps.Size(30, 30)
                    }}
                />;
        }
    }

    render() {
        return (
            <Map
                containerStyle={containerStyle}
                google={this.props.google}
                zoom={10}
                initialCenter={
                    {
                        lat: 50.450001,
                        lng: 30.523333
                    }
                }
                disableDefaultUI={true}
                onClick={this.onMapClicked}
            >
                {markers.map(marker => this.renderSwitch(marker))}
                <InfoWindow
                    marker={this.state.activeMarker}
                    visible={this.state.showingInfoWindow}>
                    <div>
                        <h4>{this.state.selectedPlace.name}</h4>
                        {this.props.token.rights >= 1 && <Button onClick={this.handleLike}><ThumbUpIcon/>{this.state.selectedPlace.nLikes}</Button>}
                        {this.props.token.rights >= 1 && <Button onClick={this.handleDislike}><ThumbDownIcon/>{this.state.selectedPlace.nDislikes}</Button>}
                    </div>
                </InfoWindow>
                {this.props.token.rights >= 1 ? <MenuIncidents token={this.props.token}/> : <div> </div>}
            </Map>
        );
    }
}

const LoadingContainer = (props) => (
    <div>Fancy loading container!</div>
)

export default GoogleApiWrapper({
    apiKey: (process.env.REACT_APP_GOOGLE_MAPS_API_KEY),
    LoadingContainer: LoadingContainer
})(MapContainer)
