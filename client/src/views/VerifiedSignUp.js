import React, {useState} from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import PropTypes from 'prop-types';
import { useFormik } from 'formik';
import * as yup from 'yup';
import {
    Container,
    Avatar,
    Typography,
    Button,
    TextField,
    Box,
    CssBaseline,
    Link,
    Grid,
    InputAdornment
} from '@mui/material';
import Geocode from "react-geocode";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import EditIcon from "@mui/icons-material/Edit";

Geocode.setApiKey(process.env.REACT_APP_GOOGLE_MAPS_GEOCODING_API_KEY);

Geocode.setLanguage("en");

const validationSchema = yup.object({
    firstName: yup
        .string('Enter your first name')
        .required('First name is required'),
    lastName: yup
        .string('Enter your last name')
        .required('Last name is required'),
    email: yup
        .string('Enter your email')
        .email('Enter a valid email')
        .required('Email is required'),
    password: yup
        .string('Enter your password')
        .min(8, 'Password should be of minimum 8 characters length')
        .required('Password is required'),
    address: yup
        .string('Enter your address')
        .required('Address is required'),
    phoneNumber: yup
        .string('Enter yout phone number')
        .required('Phone number is required'),
    ID: yup
        .string('Upload your ID')
});

export default function VerifiedSignUp({setToken}) {
    const navigate = useNavigate();
    const [image, setImage] = useState(null);

    function onImageChange(e) {
        setImage(e.target.files[0]);
    }

    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            address: '',
            phoneNumber: '',
            ID:''
        },
        validationSchema: validationSchema,
        onSubmit: values => {
            values["ID"] = image;
            values["name"]=values["firstName"]+" "+values["lastName"];
            delete values.firstName;
            delete values.lastName;
            Geocode.fromAddress(values.address).then(
                (response) => {
                    const { lat, lng } = response.results[0].geometry.location;
                    axios.post(process.env.REACT_APP_BACKEND_URL + ":" + process.env.REACT_APP_BACKEND_PORT +"/api/profiles", {
                        name: values["name"],
                        email: values["email"],
                        password: values["password"],
                        rights: 1,
                        phoneNumber: values["phoneNumber"],
                        address: {
                            lat: lat,
                            long: lng
                        }
                    }).then(response => {
                        setToken({"token" : {"_id": response.data.profile._id, "email": response.data.profile.email, "name": response.data.profile.name, "rights": response.data.profile.rights}});
                        navigate("/map");
                    })
                },
                (error) => {
                    alert(error);
                }
            );
        },
    })

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Typography component="h1" variant="h5">
                    Help Map
                </Typography>
                <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign Up Verified
                </Typography>
                <form onSubmit={formik.handleSubmit} autoComplete="on">
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                id="firstName"
                                label="First Name"
                                name="firstName"
                                type="name"
                                value={formik.values.firstName}
                                onChange={formik.handleChange}
                                error={formik.touched.firstName && Boolean(formik.errors.firstName)}
                                helperText={formik.touched.firstName && formik.errors.firstName}
                                autoFocus
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                id="lastName"
                                label="Last Name"
                                name="lastName"
                                type="name"
                                value={formik.values.lastName}
                                onChange={formik.handleChange}
                                error={formik.touched.lastName && Boolean(formik.errors.lastName)}
                                helperText={formik.touched.lastName && formik.errors.lastName}
                                autoFocus
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                id="email"
                                label="Email/Username"
                                name="email"
                                type="email"
                                autoComplete="email"
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                error={formik.touched.email && Boolean(formik.errors.email)}
                                helperText={formik.touched.email && formik.errors.email}
                                autoFocus
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                value={formik.values.password}
                                onChange={formik.handleChange}
                                error={formik.touched.password && Boolean(formik.errors.password)}
                                helperText={formik.touched.password && formik.errors.password}
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                name="phoneNumber"
                                label="Phone Number"
                                type="phone"
                                id="phoneNumber"
                                value={formik.values.phoneNumber}
                                onChange={formik.handleChange}
                                error={formik.touched.phoneNumber && Boolean(formik.errors.phoneNumber)}
                                helperText={formik.touched.phoneNumber && formik.errors.phoneNumber}
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                name="address"
                                label="Address"
                                type="address"
                                id="address"
                                value={formik.values.address}
                                onChange={formik.handleChange}
                                error={formik.touched.address && Boolean(formik.errors.address)}
                                helperText={formik.touched.address && formik.errors.address}
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <input
                                type="file"
                                accept="image/*"
                                style={{ display: 'none' }}
                                id="ID"
                                onChange={onImageChange}
                            />
                            <label htmlFor="ID">
                                <Button variant="contained" color="primary" component="span" endIcon={<CloudUploadIcon />}>
                                    Upload ID
                                </Button>
                            </label>
                        </Grid>
                        <Grid item xs={6}>
                            <Button
                                type="submit"
                                variant="contained"
                                sx={{
                                    mt: 3,
                                    mb: 2,
                                }}
                            >
                                Sign up
                            </Button>
                        </Grid>
                        <Grid item xs={6}>
                            <Link onClick={() => {
                                navigate("/");
                            }}>
                                Already have an account? Log in
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </Box>
        </Container>
    );
}

VerifiedSignUp.propTypes = {
    setToken: PropTypes.func.isRequired
}
