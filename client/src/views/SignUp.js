import React from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import PropTypes from 'prop-types';
import { useFormik } from 'formik';
import * as yup from 'yup';
import {
    Container,
    Avatar,
    Typography,
    Button,
    TextField,
    Box,
    CssBaseline,
    Link,
    Grid,
    InputAdornment
} from '@mui/material';

import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import EditIcon from "@mui/icons-material/Edit";

const validationSchema = yup.object({
    firstName: yup
        .string('Enter your first name')
        .required('First name is required'),
    lastName: yup
        .string('Enter your last name')
        .required('Last name is required'),
    email: yup
        .string('Enter your email')
        .email('Enter a valid email')
        .required('Email is required'),
    password: yup
        .string('Enter your password')
        .min(8, 'Password should be of minimum 8 characters length')
        .required('Password is required'),
});

export default function SignUp({setToken}) {
    const navigate = useNavigate();

    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
        },
        validationSchema: validationSchema,
        onSubmit: values => {
            try {
                values["rights"] = 0;
                values["name"]=values["firstName"]+" "+values["lastName"];
                delete values.firstName;
                delete values.lastName;
                axios.post(process.env.REACT_APP_BACKEND_URL + ":" + process.env.REACT_APP_BACKEND_PORT +"/api/profiles", values).then(response => {
                    setToken({"token" : {"_id": response.data.profile._id, "email": response.data.profile.email, "name": response.data.profile.name, "rights": response.data.profile.rights}});
                    navigate("/map");
                });
            } catch (error) {
                console.log(error);
            }
        },
    })


    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Typography component="h1" variant="h5">
                    Help Map
                </Typography>
                <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign Up Guest
                </Typography>
                <form onSubmit={formik.handleSubmit} autoComplete="on">
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                id="firstName"
                                label="First Name"
                                name="firstName"
                                type="name"
                                value={formik.values.firstName}
                                onChange={formik.handleChange}
                                error={formik.touched.firstName && Boolean(formik.errors.firstName)}
                                helperText={formik.touched.firstName && formik.errors.firstName}
                                autoFocus
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                id="lastName"
                                label="Last Name"
                                name="lastName"
                                type="name"
                                value={formik.values.lastName}
                                onChange={formik.handleChange}
                                error={formik.touched.lastName && Boolean(formik.errors.lastName)}
                                helperText={formik.touched.lastName && formik.errors.lastName}
                                autoFocus
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                id="email"
                                label="Email/Username"
                                name="email"
                                type="email"
                                autoComplete="email"
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                error={formik.touched.email && Boolean(formik.errors.email)}
                                helperText={formik.touched.email && formik.errors.email}
                                autoFocus
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                value={formik.values.password}
                                onChange={formik.handleChange}
                                error={formik.touched.password && Boolean(formik.errors.password)}
                                helperText={formik.touched.password && formik.errors.password}
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <Button
                                type="submit"
                                variant="contained"
                                sx={{
                                    mt: 3,
                                    mb: 2,
                                }}
                            >
                                Sign up
                            </Button>
                        </Grid>
                        <Grid item xs={6}>
                            <Button
                                type="submit"
                                variant="contained"
                                sx={{
                                    mt: 3,
                                    mb: 2,
                                    display: 'flex',
                                }}
                                onClick={() =>
                                {
                                    navigate("/signupv");
                                }}
                            >
                                Go verified
                            </Button>
                        </Grid>
                        <Grid item xs={12}>
                            <Link onClick={() => {
                                navigate("/");
                            }}>
                                Already have an account? Log in
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </Box>
        </Container>
    );
}

SignUp.propTypes = {
    setToken: PropTypes.func.isRequired
}
