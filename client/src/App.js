import React, {useEffect} from "react";
import { Route, Routes } from "react-router-dom";

import "./App.css";
import Profile from "./components/Profile";
import Map from "./views/Map";
import Footer from "./components/Footer";
import NavBar from "./components/NavBar";
import LogIn from "./views/LogIn";
import SignUp from "./views/SignUp";
import VerifiedSignUp from "./views/VerifiedSignUp";
import {useToken} from "./useToken";

const App = () => {
    const { token, setToken, deleteToken } = useToken();

    useEffect(() =>
    {
        if (!token) {
            return (
                <div id="app" className="d-flex flex-column h-100">
                    <NavBar token={token} deleteToken={deleteToken}/>
                    <LogIn setToken={setToken}/>
                    <Footer/>
                </div>
            );
        }
    })
    
    return (
        <div id="app" className="d-flex flex-column h-100">
            <NavBar token={token} deleteToken={deleteToken}/>
            <div className="container flex-grow-1">
                <div className="mt-5">
                    <Routes>
                        <Route exact path="/"  element={<LogIn setToken={setToken}/>} />
                        <Route exact path="/signup" element={<SignUp setToken={setToken}/>} />
                        <Route exact path="/profile/:id" element={<Profile token={token} setToken={setToken}/>} />
                        <Route exact path="/map" element={<Map token={token}/>} />
                        <Route exact path="/signupv" element={<VerifiedSignUp setToken={setToken}/>} />
                    </Routes>
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default App;
