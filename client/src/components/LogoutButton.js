import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import {Button} from "@mui/material";

const LogoutButton = ({deleteToken}) => {
    const { logout } = useAuth0();
    return (
        <Button
            type="submit"
            variant="contained"
            sx={{
                mt: 3,
                mb: 2
            }}
            onClick={() => {
                logout()
            }}
        >
            Log out
        </Button>
    );
};

export default LogoutButton;