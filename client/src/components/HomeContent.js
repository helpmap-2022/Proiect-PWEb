import React from "react";
import {Typography} from "@mui/material";
const HomeContent = () => (
    <Typography>
        This is an app to help people visualize the danger and safe zones in areas affected by war<br/>
        For a quick start sign up or log in.
    </Typography>
);

export default HomeContent;