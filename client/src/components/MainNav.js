import {NavLink} from "react-router-dom";
import React from "react";

const MainNav = () => (
    <div className="navbar-nav mr-auto">
        <NavLink
            to="/"
            className={(navData) => (navData.isActive ? "router-link-exact-active" : 'nav-link')}
        >
            Home
        </NavLink>
        <NavLink
            to="/profile"
            className={(navData) => (navData.isActive ? "router-link-exact-active" : 'nav-link')}
        >
            Profile
        </NavLink>
        <NavLink
            to="/external-api"
            className={(navData) => (navData.isActive ? "router-link-exact-active" : 'nav-link')}
        >
            External API
        </NavLink>
    </div>
);

export default MainNav;