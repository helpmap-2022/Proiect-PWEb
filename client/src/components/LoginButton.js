import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import {Button} from "@mui/material";

const LoginButton = () => {
    const { loginWithRedirect } = useAuth0();
    return (
        <Button
            type="submit"
            variant="contained"
            sx={{
                mt: 3,
                mb: 2,
                fontSize: 20,
                display: 'flex',
                justifyContent: 'center',
                position: 'absolute',
                left: '50%',
                top: '50%',
                transform: 'translate(-50%, -50%)',
            }}
            onClick={() => loginWithRedirect()}
        >
            Log In or Sign Up
        </Button>
    );
};

export default LoginButton;