import React from "react";
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import AddIcon from "@mui/icons-material/Add";
import redCross from "../images/red_cross.svg";
import allyTroops from "../images/allies-troops.svg";
import enemyTroops from "../images/enemies-troops.svg";
import safeHouse from "../images/safehouse.svg";
import axios from 'axios';
import Geocode from "react-geocode";

import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Fab,
    ListItemText,
    TextField
} from "@mui/material";

Geocode.setApiKey(process.env.REACT_APP_GOOGLE_MAPS_GEOCODING_API_KEY);

Geocode.setLanguage("en");

export default function MenuIncidents({token}) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [anchorRedCross, setAnchorRedCross] = React.useState(null);
    const [anchorAllies, setAnchorAllies] = React.useState(null);
    const [anchorEnemies, setAnchorEnemies] = React.useState(null);
    const [anchorSafehouse, setAnchorSafehouse] = React.useState(null);
    const open = Boolean(anchorEl);
    const openRedCrossDial = Boolean(anchorRedCross);
    const openAlliesDial = Boolean(anchorAllies);
    const openEnemiesDial = Boolean(anchorEnemies);
    const openSafehouseDial = Boolean(anchorSafehouse);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = (event) => {
        setAnchorEl(null);
    };
    const handleCloseRedCross = (event) => {
        setAnchorEl(null);
        setAnchorRedCross(event.currentTarget);
    };
    const handleCloseAllies = (event) => {
        setAnchorEl(null);
        setAnchorAllies(event.currentTarget);
    };
    const handleCloseEnemies = (event) => {
        setAnchorEl(null);
        setAnchorEnemies(event.currentTarget);
    };
    const handleCloseSafeHouse = (event) => {
        setAnchorEl(null);
        setAnchorSafehouse(event.currentTarget);
    };
    const handleCloseRedCrossDial = () => {
        setAnchorRedCross(null);
    }
    const handleCloseAlliesDial = () => {
        setAnchorAllies(null);
    }
    const handleCloseEnemiesDial = () => {
        setAnchorEnemies(null);
    }
    const handleCloseSafehouseDial = () => {
        setAnchorSafehouse(null);
    }

    const handleSubmitRedCross= (event) => {
        event.preventDefault();
        setAnchorRedCross(null);
        Geocode.fromAddress(event.target.address.value).then(
            (response) => {
                const { lat, lng } = response.results[0].geometry.location;
                axios.post(process.env.REACT_APP_BACKEND_URL + ":" + process.env.REACT_APP_BACKEND_PORT +"/api/incidents", {
                    reporter: token.email,
                    message: event.target.name.value,
                    incType: "hospital",
                    position: {
                        lat: lat,
                        long: lng
                    },
                    nLikes: 0,
                    nDislikes: 0
                }).then((response) => {
                    window.location.reload();
                }).catch(function (error) {
                    console.log(error);
                });
            },
            (error) => {
                alert(error);
            }
        );
    }

    const handleSubmitAllies = (event) => {
        event.preventDefault();
        setAnchorAllies(null);
        Geocode.fromAddress(event.target.address.value).then(
            (response) => {
                const { lat, lng } = response.results[0].geometry.location;
                axios.post(process.env.REACT_APP_BACKEND_URL + ":" + process.env.REACT_APP_BACKEND_PORT +"/api/incidents", {
                    reporter: token.email,
                    message: event.target.name.value,
                    incType: "allies",
                    position: {
                        lat: lat,
                        long: lng
                    },
                    nLikes: 0,
                    nDislikes: 0
                }).then((response) => {
                    window.location.reload();
                }).catch(function (error) {
                    console.log(error);
                });
            },
            (error) => {
                alert(error);
            }
        );
    }

    const handleSubmitEnemies = (event) => {
        event.preventDefault();
        setAnchorEnemies(null);
        Geocode.fromAddress(event.target.address.value).then(
            (response) => {
                const { lat, lng } = response.results[0].geometry.location;
                axios.post(process.env.REACT_APP_BACKEND_URL + ":" + process.env.REACT_APP_BACKEND_PORT +"/api/incidents", {
                    reporter: token.email,
                    message: event.target.name.value,
                    incType: "enemies",
                    position: {
                        lat: lat,
                        long: lng
                    },
                    nLikes: 0,
                    nDislikes: 0
                }).then((response) => {
                    window.location.reload();
                }).catch(function (error) {
                    console.log(error);
                });
            },
            (error) => {
                alert(error);
            }
        );
    }

    const handleSubmitSafehouse = (event) => {
        event.preventDefault();
        setAnchorSafehouse(null);
        Geocode.fromAddress(event.target.address.value).then(
            (response) => {
                const { lat, lng } = response.results[0].geometry.location;
                axios.post(process.env.REACT_APP_BACKEND_URL + ":" + process.env.REACT_APP_BACKEND_PORT +"/api/incidents", {
                    reporter: token.email,
                    message: event.target.name.value,
                    incType: "safehouse",
                    position: {
                        lat: lat,
                        long: lng
                    },
                    nLikes: 0,
                    nDislikes: 0
                }).then((response) => {
                    window.location.reload();
                }).catch(function (error) {
                    console.log(error);
                });
            },
            (error) => {
                alert(error);
            }
        );
    }

    return (
        <div>
            <Fab sx={{
                mt: 3,
                mb: 2,
                fontSize: 20,
                display: 'flex',
                justifyContent: 'center',
                position: 'absolute',
                left: '97%',
                top: '90%',
                transform: 'translate(-50%, -50%)',
            }} color="primary" aria-label="add" onClick={handleClick}>
                <AddIcon />
            </Fab>
            <Menu
                id="long-menu"
                MenuListProps={{
                    'aria-labelledby': 'long-button',
                }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                PaperProps={{
                    style: {
                        maxHeight: 48 * 4.5,
                        width: '20ch',
                    },
                }}
            >
                <MenuItem onClick={handleCloseRedCross}>
                    <ListItemText>Red Cross Point</ListItemText>
                    <img src={redCross} alt={"red-cross"} width={18} height={18}/>
                </MenuItem>
                <MenuItem onClick={handleCloseAllies}>
                    <ListItemText>Ally troops</ListItemText>
                    <img src={allyTroops} alt={"ally-troops"} width={35} height={35}/>
                </MenuItem>
                <MenuItem onClick={handleCloseEnemies}>
                    <ListItemText>Enemy troops</ListItemText>
                    <img src={enemyTroops} alt={"enemy-troops"} width={35} height={35}/>
                </MenuItem>
                <MenuItem onClick={handleCloseSafeHouse}>
                    <ListItemText>Safe House</ListItemText>
                    <img src={safeHouse} alt={"safe-house"} width={25} height={25}/>
                </MenuItem>
            </Menu>
            <Dialog open={openRedCrossDial} onClose={handleCloseRedCrossDial}>
                <DialogTitle>
                    Add a Red Cross point
                    <img src={redCross} alt={"red-cross"} width={18} height={18}/>
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Add a Title for the red cross point and the address of it
                    </DialogContentText>
                    <form onSubmit={handleSubmitRedCross} id="hospital-form" >
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Title"
                            type="name"
                            fullWidth
                            variant="standard"
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="address"
                            label="Address"
                            type="address"
                            fullWidth
                            variant="standard"
                        />
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseRedCrossDial}>Cancel</Button>
                    <Button type="submit" form="hospital-form">Save</Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openAlliesDial} onClose={handleCloseAlliesDial}>
                <DialogTitle>
                    Add an ally troops marker
                    <img src={allyTroops} alt={"ally-troops"} width={35} height={35}/>
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Add a Title for the ally troops and the address of it
                    </DialogContentText>
                    <form onSubmit={handleSubmitAllies} id="allies-form" >
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Title"
                            type="name"
                            fullWidth
                            variant="standard"
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="address"
                            label="Address"
                            type="address"
                            fullWidth
                            variant="standard"
                        />
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseAlliesDial}>Cancel</Button>
                    <Button type="submit" form="allies-form">Save</Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openEnemiesDial} onClose={handleCloseEnemiesDial}>
                <DialogTitle>
                    Add an enemy troops marker
                    <img src={enemyTroops} alt={"enemy-troops"} width={35} height={35}/>
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Add a Title for the enemy troops and the address of it
                    </DialogContentText>
                    <form onSubmit={handleSubmitEnemies} id="enemies-form" >
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Title"
                            type="name"
                            fullWidth
                            variant="standard"
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="address"
                            label="Address"
                            type="address"
                            fullWidth
                            variant="standard"
                        />
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseEnemiesDial}>Cancel</Button>
                    <Button type="submit" form="enemies-form">Save</Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openSafehouseDial} onClose={handleCloseSafehouseDial}>
                <DialogTitle>
                    Add a safe house marker
                    <img src={safeHouse} alt={"safe-house"} width={25} height={25}/>
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Add a Title for the safe house and the address of it
                    </DialogContentText>
                    <form onSubmit={handleSubmitSafehouse} id="safehouse-form" >
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Title"
                            type="name"
                            fullWidth
                            variant="standard"
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="address"
                            label="Address"
                            type="address"
                            fullWidth
                            variant="standard"
                        />
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseSafehouseDial}>Cancel</Button>
                    <Button type="submit" form="safehouse-form">Save</Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}