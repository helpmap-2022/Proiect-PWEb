import React from "react";
import {useNavigate} from 'react-router-dom';
import {AppBar, Box, Button, IconButton, Link, Toolbar, Typography} from "@mui/material";
import {AccountCircle} from "@mui/icons-material";


const NavBar = ({token, deleteToken}) => {
    const navigate = useNavigate();
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Link variant="h6" component="button" color='#FFFF' onClick={() => {
                        navigate("/map");
                    }}>
                        Help Map
                    </Link>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        {typeof token === 'undefined' ? " " : (token.rights===0 ? "Guest" : (token.rights===1 ? "Verified" : "Admin"))}
                    </Typography>
                    { token ? <IconButton
                        size="large"
                        aria-label="account of current user"
                        aria-controls="menu-appbar"
                        aria-haspopup="true"
                        color="inherit"
                        onClick={() => {
                            navigate("/profile");
                        }}
                    >
                        <AccountCircle/>
                    </IconButton> : <div> </div>}
                    {token ? <Button
                        type="submit"
                        variant="contained"
                        sx={{
                            mt: 3,
                            mb: 2
                        }}
                        onClick={() => {
                            deleteToken()
                            navigate("/")
                            window.location.reload()
                        }}
                    >
                        Log out
                    </Button> : <div> </div>}
                </Toolbar>
            </AppBar>
        </Box>
    );
};

export default NavBar;