import React from 'react';
import {Box, Button, Container, CssBaseline, Grid, InputAdornment, TextField, Typography} from "@mui/material";
import SaveIcon from '@mui/icons-material/Save';
import EditIcon from '@mui/icons-material/Edit';
import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import Geocode from "react-geocode";
import * as yup from "yup";
import {useFormik} from "formik";
import axios from "axios";
import {useNavigate} from "react-router-dom";

Geocode.setApiKey(process.env.REACT_APP_GOOGLE_MAPS_GEOCODING_API_KEY);

Geocode.setLanguage("en");

const validationSchema = yup.object({
    firstName: yup
        .string('Enter your first name')
        .required('First name is required'),
    lastName: yup
        .string('Enter your last name')
        .required('Last name is required'),
    email: yup
        .string('Enter your email')
        .email('Enter a valid email')
        .required('Email is required'),
    password: yup
        .string('Enter your password')
        .min(8, 'Password should be of minimum 8 characters length')
        .required('Password is required'),
    address: yup
        .string('Enter your address')
        .required('Address is required'),
    phoneNumber: yup
        .string('Enter yout phone number')
        .required('Phone number is required'),
    ID: yup
        .string('Upload your ID')
});

const Profile = ({token, setToken}) => {
    const navigate = useNavigate();

    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
            newPassword: '',
            address: '',
            phoneNumber: '',
        },
        validationSchema: validationSchema,
        onSubmit: values => {
            values["name"]=values["firstName"]+" "+values["lastName"];
            delete values.firstName;
            delete values.lastName;
            Geocode.fromAddress(values.address).then(
                (response) => {
                    const { lat, lng } = response.results[0].geometry.location;
                    axios.patch('http://localhost:5000/api/profiles/' + token.id, {
                        name: values["name"],
                        email: values["email"],
                        newPassword: values["newPassword"],
                        rights: 1,
                        phoneNumber: values["phoneNumber"],
                        address: {
                            lat: lat,
                            long: lng
                        }
                    }).then(response => {
                        setToken({"_id": response.data.profile._id, "email": response.data.profile.email, "name": response.data.profile.name, "rights": response.data.profile.rights});
                        navigate("/map");
                    });
                },
                (error) => {
                    alert(error);
                }
            );
        },
    })


    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Typography component="h1" variant="h5">
                    Your Profile
                </Typography>
                <form onSubmit={formik.handleSubmit} autoComplete="on">
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                name="email"
                                label="Email/Username"
                                type="email"
                                id="email"
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                id="newpass"
                                label="New Password"
                                name="newPassword"
                                type="password"
                                autoFocus
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                name="address"
                                label="Address"
                                type="address"
                                id="address"
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                sx={{
                                    display: 'flex'
                                }}
                                margin="normal"
                                required
                                name="phone"
                                label="Phone Number"
                                type="phoneNumber"
                                id="phone"
                                InputProps={{
                                    endAdornment: (<InputAdornment position="end">
                                        <EditIcon/>
                                    </InputAdornment>)
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <input
                                type="file"
                                accept="image/*"
                                style={{ display: 'none' }}
                                id="contained-button-file"
                            />
                            <label htmlFor="contained-button-file">
                                <Button variant="contained" color="primary" component="span" endIcon={<CloudUploadIcon />}>
                                    Upload ID
                                </Button>
                            </label>
                        </Grid>
                        <Grid item xs={6}>
                            <Button
                                type="submit"
                                variant="contained"
                                endIcon={<SaveIcon />}
                            >
                                Save
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Box>
        </Container>
    );
}

export default Profile;